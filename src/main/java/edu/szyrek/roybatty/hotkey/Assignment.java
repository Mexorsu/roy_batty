package edu.szyrek.roybatty.hotkey;

public interface Assignment extends Runnable
{
    void run();
}
